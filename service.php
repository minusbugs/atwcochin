<?php
include('layout/template.php');
?>

 <!-- Header-->
    <header data-background="assets/img/header/34.jpg" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>Services</h1>
        <h4>What we do</h4>
      </div>
    </header>
    <!-- Services Section-->
    <section id="services">
      <div class="container text-center">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <h3>Our Services</h3>
            <p>ATW’s strategically positioned business units provide fully integrated service solutions within their specialist area and combine to provide integrated global logistics, freight and value added solutions.</p>
          </div>
        </div>
        <div class="row">
          <div data-wow-delay=".2s" class="col-lg-4 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big fa fa-home"></i> Warehousing & logistic</h4>
            <p>Our core competencies are superior communication, creative strategizing and world class customer service.</p>
          </div>
          <div data-wow-delay=".4s" class="col-lg-4 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big fa fa-truck"></i> Transportation</h4>
            <p>By offering a variety of transportation modes and value-added services, clients gain end-to-end visibility and reporting on all supply chain related activity.</p>
          </div>
          <div data-wow-delay=".2s" class="col-lg-4 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big fa fa-paper-plane-o"></i> Courier Service</h4>
            <p>Conforming to our mantra 'Delivering Value', the company offers innovative and superior services worldwide.</p>
          </div>
         
        </div>
        <div class="row">
          <div data-wow-delay=".6s" class="col-lg-6 col-sm-6 wow fadeIn">
              <h4><i class="icon icon-big fa fa-tint"></i> Delear of Packged Drinking Water</h4>
              <p>We also provides a variety of customized solutions, with an eye on its customers' distinct requirements.</p>
          </div>
          <div data-wow-delay=".6s" class="col-lg-6 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big fa fa-barcode"></i>Barcode Stickering & Sim Card Processing</h4>
            <p>We also provides barcode stickering,sim card processing and all type of batch code printing.</p>
          </div>
        </div>
      </div>
    </section>
    <!-- Services 2 Section-->
    <!-- <section class="section-small bg-img4 text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div data-wow-delay=".2s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big icon ion-ios-infinite-outline"></i>LIFETIME UPDATES</h4>
            <p>Lorem ipsum dolor sit amet consecte tur adipiscingtitor diam</p>
          </div>
          <div data-wow-delay=".4s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-stopwatch-outline"></i>LIGHTWEIGHT</h4>
            <p>Lorem ipsum dolor sit amet consecte tur adipiscingtitor diam</p>
          </div>
          <div data-wow-delay=".6s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-monitor-outline"></i>RESPONSIVE</h4>
            <p>Lorem ipsum dolor sit amet consecte tur adipiscingtitor diam</p>
          </div>
          <div data-wow-delay=".8s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big ion-ios-settings"></i>MULTI-PURPOSE</h4>
            <p>Lorem ipsum dolor sit amet consecte tur adipiscingtitor diam</p>
          </div>
        </div>
      </div>
    </section> -->
    <!-- Why Section-->
    <section>
      <div class="container text-center">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <h3>Why Choose Us?</h3>
          </div>
        </div>
        <div class="row">
          <div data-wow-delay=".2s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon-big icon ion-ios-infinite-outline"></i> Global Strength</h4>
            <p>By placing your trust in the INDIA'S LEADING LOGISTIC  COMPANY, you benefit from its financial stability, massive resources and buying power, heavy investment in supply chain technology and global reach. Wherever you need to operate, ATW will be there to provide globally standardized, cost-efficient, high-quality, innovative solutions.</p>
          </div>
          <div data-wow-delay=".4s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon-big ion-ios-sunny-outline"></i> IT Philosophy </h4>
            <p>As the supply chain industry’s leader in the implementation of warehouse management and transport management systems, ATW is constantly reviewing and deploying new technological capabilities as they become available. Our IT philosophy drives us to maximize value to customers, providing excellent project implementation and management, fast and repeatable deployment and continuous improvement.</p>
          </div>
          <div data-wow-delay=".6s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon-big ion-ios-glasses-outline"></i> Responsible Living</h4>
            <p>As a socially responsible company, you expect your providers to work responsibly too.idea we are committed to making a difference in people’s lives. Our initiatives help protect the planet and focus on making your operations more sustainable. Additionally, you can count on us to implement and uphold a ‘Safety First’ culture that will protect both your people and your brand.</p>
          </div>
          <div data-wow-delay=".8s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon-big ion-ios-pulse"></i> Deep Sector Understanding</h4>
            <p>Understanding, predicting and responding effectively to the trends and challenges facing your industry sector is vital to the success of your business.  Our logistics experts, who focus on these sector market challenges, can help you develop strategies to meet both your current and future supply chain needs.</p>
          </div>
        </div>
      </div>
    </section> 
    <!-- Footer Section-->   
    <section class="section-small footer lesspadding">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <h6>Powered By <a href="http://minusbugs.com"> minusbugs.com</a>
            </h6>
          </div>
          <div class="col-sm-3 col-sm-offset-1">
            <h6>We <i class="fa fa-heart fa-fw"></i> creative people
            </h6>
          </div>
          <div class="col-sm-3 col-sm-offset-1 text-right">
            <ul class="list-inline">
              <li><a href="/"><i class="fa fa-twitter fa-fw fa-lg"></i></a></li>
              <li><a href="/"><i class="fa fa-facebook fa-fw fa-lg"></i></a></li>
              <li><a href="/"><i class="fa fa-google-plus fa-fw fa-lg"></i></a></li>
              <li><a href="/"><i class="fa fa-linkedin fa-fw fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>