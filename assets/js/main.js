//full name validation
function validateName(fullnameField) 
{
  if (fullnameField.value.length < 2) {
    document.getElementById("reachus_pagefullname").innerHTML = "Enter at least 2 letters";
    $("#name").css({
      "border": "1px solid red",
    });
     return false;
  }
  else {
    document.getElementById("reachus_pagefullname").innerHTML = "";
    $("#name").css({
      "border": "1px solid #ccc",

    });
    return true;
  }
}
//end

//email validation
function validateEmail(emailField) 
{
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    document.getElementById("reachus_pageemail").innerHTML = "Enter valid email Id";
    $("#email").css({
      "border": "1px solid red",
    });
     return false;
  }
  if (reg.test(emailField.value) == true) {
    document.getElementById("reachus_pageemail").innerHTML = "";
    $("#email").css({
      "border": "1px solid #ccc",

    });
    return true;
  }
}
//end

//message validation
function validateMessage(messageField) 
{
  if (messageField.value.length < 9) {
    document.getElementById("reachus_pagemessage").innerHTML = "Enter at least 10 letters";
    $("#message").css({
      "border": "1px solid red",
    });
    return false;
  }
  else {
    document.getElementById("reachus_pagemessage").innerHTML = "";
    $("#message").css({
      "border": "1px solid #ccc",

    });
    return true;
  }
}
//end

//mobile number validation
function numbervalidate(textbox, e) 
{

  var charCode = (e.which) ? e.which : e.keyCode;
  if (charCode == 46 || charCode > 31 && (charCode < 48 || charCode > 57)) {
    document.getElementById("reachus_pagephonenumber").innerHTML = "Numbers only";
    $("#phone").css({
      "border": "1px solid red",
    });
    return false;
  } else {
    document.getElementById("reachus_pagephonenumber").innerHTML = "";
    $("#phone").css({
      "border": "1px solid rgb(0,255,255)",

    });
    return true;
  }
}
//end

//submitcontactform
function submitcontactform()
{
  var name=$("#name").val();
  var email=$("#email").val();
  var phone=$("#phone").val();
  var message=$("#message").val();
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  if( name==""|| email==""|| phone==""|| message=="")
  {
    $("#ajax-alert").addClass("alert  alert-danger").text("Enter every fields");
    $("#ajax-alert").alert();
    $("#ajax-alert").fadeTo(3000, 3000).fadeOut(500, function(){}); 
  }
  else 
    if(name.length < 2)
    {      
      $("#ajax-alert").addClass("alert  alert-danger").text("Enter at least 2 characters in name");
      $("#ajax-alert").alert();
      $("#ajax-alert").fadeTo(3000, 3000).fadeOut(500, function(){}); 
    }
  else
    if (reg.test(email) == false)
    {
       $("#ajax-alert").addClass("alert  alert-danger").text("Enter valid email address");
       $("#ajax-alert").alert();
       $("#ajax-alert").fadeTo(3000, 3000).fadeOut(500, function(){}); 
    }
    else 
      if(phone.length != 10)
      {      
        $("#ajax-alert").addClass("alert  alert-danger").text("Enter valid phone number");
        $("#ajax-alert").alert();
        $("#ajax-alert").fadeTo(3000, 3000).fadeOut(500, function(){}); 
      }
    else 
      if(message.length < 11)
      {      
        $("#ajax-alert").addClass("alert  alert-danger").text("Enter at least 10 characters in message");
        $("#ajax-alert").alert();
        $("#ajax-alert").fadeTo(3000, 3000).fadeOut(500, function(){}); 
      }
      else
      {
        //data = {name:name,email:email,phone:phone,message:message};
        url ="../atw-cochin/assets/mail/contact.php";
        $.ajax({
                type: "POST",
                data: {Name:name,Email:email,Phone:phone,Message:message},
                url: url,
                dataType: 'json',
                success: function(result) {
                  if(result)
                  {
                    //alert(result);
                    $("#ajax-alert").addClass("alert  alert-success").text("Mail Hasbeen Send");
                    $("#ajax-alert").alert();
                    $("#ajax-alert").fadeTo(3000, 3000).fadeOut(500, function(){}); 
                    $('#name').val("");
                    $('#email').val("");
                    $('#phone').val("");
                    $('#message').val("");
                  }
                  else
                  {
                    //alert(result);
                    $('#name').val("");
                    $('#email').val("");
                    $('#phone').val("");
                    $('#message').val("");
                  }
                }
              });
      }
}
//end