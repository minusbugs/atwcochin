<?php
include('layout/template.php');
?>

<!-- Header-->
    <header data-background="assets/img/header/36.jpg" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>About Us</h1>
        <h4>Who we are?</h4>
      </div>
    </header>
    <!-- Slider-->
    <section id="about" class="section-small">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <h3>Who We Are</h3>
            <p class="no-pad">ATW Cochin is the India’s leading contract logistics provider. Combining value-added and management services with traditional fulfilment and distribution, our customized, integrated logistics solutions drive efficiency, improve quality and create competitive advantage..</p>
            <h2 class="classic">The ATW Team</h2>
          </div>
          <div data-wow-duration="2s" data-wow-delay=".2s" class="col-lg-5 col-lg-offset-1 wow zoomIn">
            <div id="carousel-light2" class="carousel slide carousel-fade">
              <ol class="carousel-indicators">
                <li data-target="#carousel-light2" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-light2" data-slide-to="1"></li>
                <li data-target="#carousel-light2" data-slide-to="2"></li>
              </ol>
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="assets/img/misc/33.jpg" alt="" class="img-responsive center-block"></div>
                <div class="item"><img src="assets/img/misc/34.jpg" alt="" class="img-responsive center-block"></div>
                <div class="item"><img src="assets/img/misc/35.jpg" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>    
    <!-- Why Section-->
    <section>
      <div class="container text-center">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <h3>Why Choose Us?</h3>
          </div>
        </div>
        <div class="row">
          <div data-wow-delay=".2s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon-big icon ion-ios-infinite-outline"></i> Global Strength</h4>
            <p>By placing your trust in the INDIA'S LEADING LOGISTIC  COMPANY, you benefit from its financial stability, massive resources and buying power, heavy investment in supply chain technology and global reach. Wherever you need to operate, ATW will be there to provide globally standardized, cost-efficient, high-quality, innovative solutions.</p>
          </div>
          <div data-wow-delay=".4s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon-big ion-ios-sunny-outline"></i> IT Philosophy </h4>
            <p>As the supply chain industry’s leader in the implementation of warehouse management and transport management systems, ATW is constantly reviewing and deploying new technological capabilities as they become available. Our IT philosophy drives us to maximize value to customers, providing excellent project implementation and management, fast and repeatable deployment and continuous improvement.</p>
          </div>
          <div data-wow-delay=".6s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon-big ion-ios-glasses-outline"></i> Responsible Living</h4>
            <p>As a socially responsible company, you expect your providers to work responsibly too.Idea we are committed to making a difference in people’s lives. Our initiatives help protect the planet and focus on making your operations more sustainable. Additionally, you can count on us to implement and uphold a ‘Safety First’ culture that will protect both your people and your brand.</p>
          </div>
          <div data-wow-delay=".8s" class="col-lg-3 col-sm-6 wow fadeIn">
            <h4><i class="icon-big ion-ios-pulse"></i> Deep Sector Understanding</h4>
            <p>Understanding, predicting and responding effectively to the trends and challenges facing your industry sector is vital to the success of your business.  Our logistics experts, who focus on these sector market challenges, can help you develop strategies to meet both your current and future supply chain needs.</p>
          </div>
        </div>
      </div>
    </section>    
    <!-- Footer Section-->    
    <section class="section-small footer lesspadding">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <h6>Powered By <a href="http://minusbugs.com"> minusbugs.com</a>
            </h6>
          </div>
          <div class="col-sm-3 col-sm-offset-1">
            <h6>We <i class="fa fa-heart fa-fw"></i> creative people
            </h6>
          </div>
          <div class="col-sm-3 col-sm-offset-1 text-right">
            <ul class="list-inline">
              <li><a href="/"><i class="fa fa-twitter fa-fw fa-lg"></i></a></li>
              <li><a href="/"><i class="fa fa-facebook fa-fw fa-lg"></i></a></li>
              <li><a href="/"><i class="fa fa-google-plus fa-fw fa-lg"></i></a></li>
              <li><a href="/"><i class="fa fa-linkedin fa-fw fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>