<?php
include('layout/template.php');
?>

<!-- Header-->
    <header data-background="assets/img/header/32.jpg" class="intro">
      <!-- Intro Header-->
      <div class="intro-body">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <h1><span class="rotate">ATW Cochin</span></h1>
              <p>Our Warehousing Solutions improve inventory efficiency and accelerate your response to changing customer demand. Our experts design, implement, and operate flexible warehousing and distribution solutions tailored to your business needs. They analyze every point in your supply chain to determine the optimal solution.</p>
              <h2 class="classic">The ATW Team</h2>
              <div data-wow-delay="1s" class="scroll-btn hidden-xs wow fadeInDown"><a href="#action-slider2" class="page-scroll"><span class="mouse"><span class="weel"><span></span></span></span></a></div>
            </div>
          </div>
        </div>
      </div>
    </header>
<!-- Slider-->
    <section id="action-slider2" class="bg-dark">
      <div class="container">
        <div class="row">
          <div class="col-lg-5">
            <h3>Who we are?</h3>
            <p>ATW Cochin is the indas's leading contract logistics provider. Combining value-added and management services with traditional fulfilment and distribution, our customized, integrated logistics solutions drive efficiency, improve quality and create competitive advantage..</p><a href="about_us.php" class="btn btn-gray">READ MORE</a>
          </div>
          <div class="col-lg-6 col-lg-offset-1">
            <div id="carousel-light3" class="carousel slide carousel-fade">
              <ol class="carousel-indicators">
                <li data-target="#carousel-light3" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-light3" data-slide-to="1"></li>
                <li data-target="#carousel-light3" data-slide-to="2"></li>
              </ol>
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="assets/img/misc/33.jpg" alt="" class="img-responsive center-block"></div>
                <div class="item"><img src="assets/img/misc/34.jpg" alt="" class="img-responsive center-block"></div>
                <div class="item"><img src="assets/img/misc/35.jpg" alt="" class="img-responsive center-block"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Slider Section-->
        <!-- Services Section-->
    <section id="services" class="bg-dark2">
      <div class="container text-center">
        <div class="row">
          <div class="col-lg-8 col-lg-offset-2">
            <h3>Our Services</h3>
            <p>ATW’s strategically positioned business units provide fully integrated service solutions within their specialist area and combine to provide integrated global logistics, freight and value added solutions.</p>
          </div>
        </div>
        <div class="row">
          <div data-wow-delay=".2s" class="col-lg-4 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big fa fa-home"></i> Warehousing & logistic</h4>
            <p>Our core competencies are superior communication, creative strategizing and world class customer service.</p>
          </div>
          <div data-wow-delay=".4s" class="col-lg-4 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big fa fa-truck"></i> Transportation</h4>
            <p>By offering a variety of transportation modes and value-added services, clients gain end-to-end visibility and reporting on all supply chain related activity.</p>
          </div>
          <div data-wow-delay=".2s" class="col-lg-4 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big fa fa-paper-plane-o"></i> Courier Service</h4>
            <p>Conforming to our mantra 'Delivering Value', the company offers innovative and superior services worldwide.</p>
          </div>
         
        </div>
        
        <div class="row">
          <div data-wow-delay=".6s" class="col-lg-6 col-sm-6 wow fadeIn">
              <h4><i class="icon icon-big fa fa-tint"></i> Delear of Packged Drinking Water</h4>
              <p>We also provides a variety of customized solutions, with an eye on its customers' distinct requirements.</p>
          </div>
          <div data-wow-delay=".6s" class="col-lg-6 col-sm-6 wow fadeIn">
            <h4><i class="icon icon-big fa fa-barcode"></i>Barcode Stickering & Sim Card Processing</h4>
            <p>We also provides barcode stickering,sim card processing and all type of batch code printing.</p>
          </div>
        </div>
        
      </div>
    </section>
    <!-- Testimonials Section-->
    <section id="testimonials" class="bg-dark">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <h3 class="text-center">Testimonials</h3>
            <div id="carousel-testimonials" data-ride="carousel" class="carousel slide carousel-fade">
              <!-- Indicators-->
              <ol class="carousel-indicators">
                <li data-target="#carousel-testimonials" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-testimonials" data-slide-to="1"></li>
                <li data-target="#carousel-testimonials" data-slide-to="2"></li>
              </ol>
              <!-- Wrapper for slides-->
              <div role="listbox" class="carousel-inner">
                <div class="item active"><img src="assets/img/testimonials/avatar1.jpg" alt="" class="center-block">
                  <div class="carousel-caption">
                    <h2 class="classic">Richard Kindall</h2>
                    <h5 class="no-pad">Thank you very much, the service is great! Very concise and simple.</h5>
                  </div>
                </div>
                <div class="item"><img src="assets/img/testimonials/avatar1.jpg" alt="" class="center-block">
                  <div class="carousel-caption">
                    <h2 class="classic">David Cohen</h2>
                    <h5 class="no-pad">We love the approachable format and the fact that they chose to feature customers that users can really relate to.</h5>
                  </div>
                </div>
                <div class="item"><img src="assets/img/testimonials/avatar1.jpg" alt="" class="center-block">
                  <div class="carousel-caption">
                    <h2 class="classic">Jane Navarro</h2>
                    <h5 class="no-pad">Amazing and SUPER customer service, looks like a great responsible courier service providers in india.</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Contact Section-->
    <section id="contact" class="bg-img2">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <h3>contact us</h3>
            <p>Feel free to contact us. A business has to be involving, it has to be fun, and it has to exercise your creative instincts. Start where you are. Use what you have. Do what you can. </p>
            <hr>
            <h5><i class="fa fa-map-marker fa-fw fa-lg"></i> ATHUL TRADING & WAREHOUSING (P)LTD<br> Door No.15/730, Kuruppanezhathu House<br>Poothotta P O, Ernakulam-682 307
            </h5>
            <h5><i class="fa fa-envelope fa-fw fa-lg"></i> athulenterprises@gmail.com
            </h5>
            <h5><i class="fa fa-phone fa-fw fa-lg"></i> 8606 123 457
            </h5>
          </div>
          <div class="col-md-5 col-md-offset-2">
            <h3>Say hello</h3>
            <!-- Contact Form - Enter your email address on line 17 of the mail/contact_me.php file to make this form work. For more information on how to do this please visit the Docs!-->            

            <form class="dark-form">
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="name" class="sr-only control-label">You Name</label>
                  <input maxlength="40" id="name" type="text" placeholder="Your Name" required="" class="form-control input-lg" onblur="validateName(this)" onkeypress="validateName(this)"><span class="help-block text-danger" id="reachus_pagefullname"></span>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="email" class="sr-only control-label">You Email</label>
                  <input id="email" type="email" placeholder="You Email" required=""  class="form-control input-lg" onblur="validateEmail(this)" onkeypress="validateEmail(this)"><span class="help-block text-danger" id="reachus_pageemail"></span>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="phone" class="sr-only control-label">You Phone</label>
                  <input id="phone" maxlength="10" onblur="return numbervalidate(this,event);" onkeypress="return numbervalidate(this,event);" type="text" placeholder="You Phone" required=""  class="form-control input-lg"><span class="help-block text-danger" id="reachus_pagephonenumber"></span>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="message" class="sr-only control-label">Message</label>
                  <textarea id="message" rows="2" placeholder="Message" required=""  aria-invalid="false" class="form-control input-lg" maxlength="250"  onblur="validateMessage(this)" onkeypress="validateMessage(this)"></textarea><span class="help-block text-danger" id="reachus_pagemessage"></span>
                </div>
              </div>
              <div id="success"></div>
              <button type="button" class="btn btn-lg btn-dark" onclick="submitcontactform()">Send</button>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- footer small-->
    <div class="container-fluid footer-small bg-dark text-center">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <p data-wow-iteration="999" data-wow-duration="3s" class="wow flash"><a href="#page-top" class="page-scroll"><i class="icon ion-ios-arrow-up fa-2x"></i></a></p>
          <ul class="list-inline">
            <li><a href="/"><i class="fa fa-twitter fa-fw fa-2x"></i></a></li>
            <li><a href="/"><i class="fa fa-facebook fa-fw fa-2x"></i></a></li>
            <li><a href="/"><i class="fa fa-google-plus fa-fw fa-2x"></i></a></li>
            <li><a href="/"><i class="fa fa-linkedin fa-fw fa-2x"></i></a></li>
          </ul>
          <!-- <p><a href="https://themeforest.net/item/universal-smart-multipurpose-html5-template/17268942" class="btn btn-lg btn-gray">Purchase now</a></p> -->
          <p class="small"><a href="http://minusbugs.com">©2017 <i class="fa fa-heart fa-fw"></i>MINUSBUGS.COM</a></p>
        </div>
      </div>
    </div>