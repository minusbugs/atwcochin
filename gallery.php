<?php
include('layout/template.php');
?>

 <!-- Header-->
    <header data-background="assets/img/header/35.jpg" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>Gallery</h1>        
      </div>
    </header>
    <!-- Portfolio-->
    <section id="portfolio">
      <div class="container text-center">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <h3>Latest Works</h3>
            <!-- <ul class="portfolio-sorting list-inline text-center">
              <li><a href="portfolio-single.html" data-group="all" class="active">All</a></li>
              <li><a href="portfolio-single.html" data-group="design">Design</a></li>
              <li><a href="portfolio-single.html" data-group="photo">Photo</a></li>
              <li><a href="portfolio-single.html" data-group="branding">Branding</a></li>
            </ul> -->
          </div>
        </div>
      </div>
      <div class="container">
        <div id="grid" class="row portfolio-items">
          <div data-groups="[&quot;design&quot;, &quot;branding&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/18.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div>
          <div data-groups="[&quot;photo&quot;, &quot;branding&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/19.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div>
          <div data-groups="[&quot;&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/20.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div>
          <div data-groups="[&quot;design&quot;, &quot;photo&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/21.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div>
          <div data-groups="[&quot;branding&quot;, &quot;design&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/22.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div>
          <div data-groups="[&quot;photo&quot;, &quot;design&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/23.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div>
          <div data-groups="[&quot;photo&quot;, &quot;branding&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/24.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div>
          <div data-groups="[&quot;design&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/25.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div>
          <!-- <div data-groups="[&quot;photo&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/121.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div> -->
          <div data-groups="[&quot;photo&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/26.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div>
          <div data-groups="[&quot;design&quot;, &quot;branding&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/27.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div>
           <div data-groups="[&quot;design&quot;, &quot;branding&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/28.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div>
           <div data-groups="[&quot;design&quot;, &quot;branding&quot;]" class="col-sm-4 no-pad">
            <div class="portfolio-item"><a href="#"><img src="assets/img/portfolio/29.jpg" alt="">
                <div class="portfolio-overlay">
                  <div class="caption">
                    <h5>ATW Cochin</h5><span>Warehousing solutions</span>
                  </div>
                </div></a></div>
          </div>   
        </div>
      </div>
    </section>
   
  <!-- Footer Section-->    
    <section class="section-small footer lesspadding">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <h6>Powered By <a href="http://minusbugs.com"> minusbugs.com</a>
            </h6>
          </div>
          <div class="col-sm-3 col-sm-offset-1">
            <h6>We <i class="fa fa-heart fa-fw"></i> creative people
            </h6>
          </div>
          <div class="col-sm-3 col-sm-offset-1 text-right">
            <ul class="list-inline">
              <li><a href="/"><i class="fa fa-twitter fa-fw fa-lg"></i></a></li>
              <li><a href="/"><i class="fa fa-facebook fa-fw fa-lg"></i></a></li>
              <li><a href="/"><i class="fa fa-google-plus fa-fw fa-lg"></i></a></li>
              <li><a href="/"><i class="fa fa-linkedin fa-fw fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>