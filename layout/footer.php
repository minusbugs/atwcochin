<!-- jQuery-->
    <script src="assets/js/jquery-1.12.4.min.js"></script>
    <!-- Bootstrap Core JavaScript-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Plugin JavaScript-->
    <script src="assets/js/jquery.easing.min.js"></script>
    <script src="assets/js/jquery.countdown.min.js"></script>
    <script src="assets/js/device.min.js"></script>
    <!-- <script src="assets/js/form.min.js"></script> -->
    <script src="assets/js/jquery.placeholder.min.js"></script>
    <script src="assets/js/jquery.shuffle.min.js"></script>
    <script src="assets/js/jquery.parallax.min.js"></script>
    <script src="assets/js/jquery.circle-progress.min.js"></script>
    <script src="assets/js/jquery.swipebox.min.js"></script>
    <script src="assets/js/smoothscroll.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.smartmenus.js"></script>
    <script src="assets/js/text-rotator.min.js"></script>
    <!-- Custom Theme JavaScript-->
    <script src="assets/js/universal.js"></script>
    <script src="assets/js/main.js"></script>
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </body>
</html>