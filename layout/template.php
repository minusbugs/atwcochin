<?php
include('layout/header.php');
?>

    <!-- Navigation-->
    <nav class="navbar navbar-universal navbar-custom navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target=".navbar-main-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="#page-top" class="navbar-brand page-scroll">
            <!-- Text or Image logo--><img src="assets/img/logotransparent.png" alt="Logo" class="logo"><img src="assets/img/logotransparent.png" alt="Logo" class="logodark"></a>
        </div>
        <div class="collapse navbar-collapse navbar-main-collapse">
          <ul class="nav navbar-nav navbar-left">
            <li class="hidden"><a href="#page-top"></a></li>
            <li><a href="index.php">Home <span class="caret"></span></a>
             
            </li>
            <li><a href="about_us.php">About Us <span class="caret"></span></a>
              
            </li>
            <li><a href="service.php">Services <span class="caret"></span></a>
              
            </li>
            <li><a href="gallery.php">Gallery <span class="caret"></span></a>
              
            </li>
            <li><a href="contact_us.php">Contact Us <span class="caret"></span></a>
             
            </li> 
            
          </ul>
        </div>
      </div>
    </nav>

<?php
include('layout/footer.php');
?>