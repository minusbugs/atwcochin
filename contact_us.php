<?php
include('layout/template.php');
?>

<!-- Header-->
    <header data-background="assets/img/header/33.jpg" class="intro introhalf">
      <!-- Intro Header-->
      <div class="intro-body">
        <h1>Contact us</h1>
        <h4>SAY HELLO</h4>
      </div>
    </header>
    <!-- Contact Section-->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-md-5">
            <h3>contact us</h3>
            <p>Feel free to contact us. A business has to be involving, it has to be fun, and it has to exercise your creative instincts. Start where you are. Use what you have. Do what you can. </p>
            <hr>
            <h5><i class="fa fa-map-marker fa-fw fa-lg"></i> ATHUL TRADING & WAREHOUSING (P)LTD<br> Door No.15/730, Kuruppanezhathu House<br>Poothotta P O, Ernakulam-682 307
            </h5>
            <h5><i class="fa fa-envelope fa-fw fa-lg"></i> athulenterprises@gmail.com
            </h5>
            <h5><i class="fa fa-phone fa-fw fa-lg"></i> 8606 123 457
            </h5>
          </div>
          <div class="col-md-5 col-md-offset-2">
            <h3>Say hello</h3>
            <!-- Contact Form - Enter your email address on line 17 of the mail/contact_me.php file to make this form work. For more information on how to do this please visit the Docs!-->
            <form>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="name" class="sr-only control-label">You Name</label>
                  <input maxlength="40" id="name" type="text" placeholder="Your Name" required="" class="form-control input-lg" onblur="validateName(this)" onkeypress="validateName(this)"><span class="help-block text-danger" id="reachus_pagefullname"></span>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="email" class="sr-only control-label">You Email</label>
                  <input id="email" type="email" placeholder="You Email" required=""  class="form-control input-lg" onblur="validateEmail(this)" onkeypress="validateEmail(this)"><span class="help-block text-danger" id="reachus_pageemail"></span>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="phone" class="sr-only control-label">You Phone</label>
                  <input id="phone" maxlength="10" onblur="return numbervalidate(this,event);" onkeypress="return numbervalidate(this,event);" type="text" placeholder="You Phone" required=""  class="form-control input-lg"><span class="help-block text-danger" id="reachus_pagephonenumber"></span>
                </div>
              </div>
              <div class="control-group">
                <div class="form-group floating-label-form-group controls">
                  <label for="message" class="sr-only control-label">Message</label>
                  <textarea id="message" rows="2" placeholder="Message" required=""  aria-invalid="false" class="form-control input-lg" maxlength="250"  onblur="validateMessage(this)" onkeypress="validateMessage(this)"></textarea><span class="help-block text-danger" id="reachus_pagemessage"></span>
                </div>
              </div>
              <div id="success"></div>
              <button type="button" class="btn btn-dark" onclick="submitcontactform()">Send</button>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- Map Section-->
    <div id="map"></div>
    <!-- Footer Section-->
    <section class="section-small footer lesspadding">
      <div class="container">
        <div class="row">
          <div class="col-sm-4">
            <h6>Powered By <a href="http://minusbugs.com"> minusbugs.com</a>
            </h6>
          </div>
          <div class="col-sm-3 col-sm-offset-1">
            <h6>We <i class="fa fa-heart fa-fw"></i> creative people
            </h6>
          </div>
          <div class="col-sm-3 col-sm-offset-1 text-right">
            <ul class="list-inline">
              <li><a href="/"><i class="fa fa-twitter fa-fw fa-lg"></i></a></li>
              <li><a href="/"><i class="fa fa-facebook fa-fw fa-lg"></i></a></li>
              <li><a href="/"><i class="fa fa-google-plus fa-fw fa-lg"></i></a></li>
              <li><a href="/"><i class="fa fa-linkedin fa-fw fa-lg"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3E86i8mx1BZDlAaLcknh_mWl4F70i4os"></script>
    <script src="assets/js/map.js"></script>